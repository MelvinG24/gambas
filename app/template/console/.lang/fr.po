#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: console 3.8.90\n"
"POT-Creation-Date: 2019-05-18 17:10 UTC\n"
"PO-Revision-Date: 2015-09-17 19:09 UTC\n"
"Last-Translator: Benoît Minisini <gambas@users.sourceforge.net>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: .project:1
msgid "Command-line application"
msgstr "Application en ligne de commande"

#: .project:2
msgid "A command-line application without graphical user interface and using no component."
msgstr "Une application en ligne de commande sans interface graphique, et n'utilisant aucun composant."
