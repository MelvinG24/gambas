# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
# Translators:
# Дмитрий Ошкало <dmitry.oshkalo@gmail.com>, 2019
# Kашицин Роман <calorus@gmail.com>, 2019
# Олег o1hk <o1h2k3@yandex.ru>, 2019
# AlexL <loginov.alex.valer@gmail.com>, 2019
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-23 04:17+0300\n"
"PO-Revision-Date: 2019-05-09 00:48+0000\n"
"Last-Translator: AlexL <loginov.alex.valer@gmail.com>, 2019\n"
"Language-Team: Russian (https://www.transifex.com/rus-open-source/teams/44267/ru/)\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);\n"

#: app/examples/Games/BeastScroll/.project:20
msgid "BeastScroll"
msgstr "Свиток зверя"

#: app/examples/Games/BeastScroll/.project:21
msgid ""
"Parallax scrolling demo.\n"
"\n"
"This example shows a parallax scrolling with a music playing in the background. It is based on the SDL2 components."
msgstr ""
"Параллакс-скроллинг демо.\n"
"\n"
"В этом примере показана прокрутка параллакса с фоновой музыкой. Он основан на компонентах SDL2."

#: app/examples/Games/BeastScroll/.src/MMain.module:190
msgid "FPS"
msgstr "кадр/с"

#: app/examples/Games/BeastScroll/.src/MMain.module:192
msgid "Toggle fullscreen"
msgstr "Переключить полный экран"

#: app/examples/Games/BeastScroll/.src/MMain.module:193
msgid "Take screenshot to ~/BeastScroll.png"
msgstr "Сделать скриншот в ~/BeastScroll.png"

#: app/examples/Games/BeastScroll/.src/MMain.module:194
msgid "Quit"
msgstr "Выход"

