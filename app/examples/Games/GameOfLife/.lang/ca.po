msgid ""
msgstr ""
"Project-Id-Version: GameOfLife\n"
"POT-Creation-Date: 2002-11-01 04:27+0100\n"
"PO-Revision-Date: 2011-03-20 17:58+0100\n"
"Last-Translator: Jordi Sayol <g.sayol@yahoo.es>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Poedit-Language: Catalan\n"

#: FMain.class:265
msgid "0 Neighbour"
msgstr "0 veïns"

#: FMain.class:258
msgid "1 Neighbour"
msgstr "1 veí"

#: FMain.class:251
msgid "2 Neighbours"
msgstr "2 veïns"

#: FMain.class:245
msgid "3 Neighbours"
msgstr "3 veïns"

#: FMain.class:238
msgid "4 Neighbours"
msgstr "4 veïns"

#: FMain.class:231
msgid "5 Neighbours"
msgstr "5 veïns"

#: FMain.class:224
msgid "6 Neighbours"
msgstr "6 veïns"

#: FMain.class:217
msgid "7 Neighbours"
msgstr "7 veïns"

#: FMain.class:210
msgid "8 Neighbours"
msgstr "8 veïns"

#: FMain.class:291
msgid "Alive"
msgstr "Viu"

#: FMain.class:296
msgid "Dead"
msgstr "Mort"

#: FMain.class:302
msgid "Draw cell borders"
msgstr "Dibuixa la vora de les ceŀles"

#: FMain.class:72
msgid "Evolution Delay: "
msgstr "Retard d'evolució:"

#: FMain.class:199
msgid "Evolution Delay: 500ms"
msgstr "Retard d'evolució: 500ms"

#: .project:1
msgid "Game of Life"
msgstr "Joc de la Vida"

#: FMain.class:147
msgid "GameOfLife"
msgstr "JocDeLaVida"

#: FMain.class:317
msgid "Horizontal symmetry"
msgstr "Simetria horitzontal"

#: FMain.class:276
msgid "Options"
msgstr "Opcions"

#: FMain.class:272
msgid "Select the Count of Neighbours where a cell will die or keep its state."
msgstr "Seleccioneu el comptatge de veïns on una cèŀlula morirà o mantindrà el seu estat."

#: FMain.class:280
msgid "Set here the probability that a Cell will be born or not when you spawn the first Generation."
msgstr "Ajusteu aquí la probabilitat de que una ceŀlula neixi o no quan s'engendra la primera generació."

#: FMain.class:307
msgid "Small generation"
msgstr "Generació petita"

#: FMain.class:176
msgid "Spawn First Generation"
msgstr "Engendra la primera generació"

#: FMain.class:194
msgid "Start Evolution"
msgstr "Inicia l'evolució"

#: FMain.class:206
msgid "Survival"
msgstr "Supervivència"

#: FMain.class:136
msgid "The Game of Life"
msgstr "El Joc de la Vida"

#: FMain.class:312
msgid "Vertical symmetry"
msgstr "Simetria vertical"

#: FMain.class:164
msgid ""
"Written in Gambas<br>\n"
"by <b>Iman Karim</b><br>\n"
"and <b>Benoît Minisini</b>\n"
"<p>\n"
"<i>Thanks to the Gambas team!</i>"
msgstr ""
"Escrit al Gambas<br>\n"
"per <b>Iman Karim</b><br>\n"
"i <b>Benoît Minisini</b>\n"
"<p>\n"
"<i>Gràcies a l'equip del Gambas!</i>"

